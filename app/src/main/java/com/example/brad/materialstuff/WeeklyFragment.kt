package com.example.brad.materialstuff

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.brad.materialstuff.WeatherModels.WeatherData
import com.example.brad.materialstuff.adapters.DailyAdapter

class WeeklyFragment : Fragment(){

    lateinit var recyclerView: RecyclerView
    lateinit var viewAdapter: DailyAdapter
    lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.weekly_layout, container, false)

        viewManager = LinearLayoutManager(context)
        viewAdapter = DailyAdapter(WeatherData().getFakeWeekly())

        recyclerView = view.findViewById<RecyclerView>(R.id.weekly_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        return view
    }
}