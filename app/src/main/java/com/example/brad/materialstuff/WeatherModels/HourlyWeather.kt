package com.example.brad.materialstuff.WeatherModels

import java.util.*

data class HourlyWeather(val date: Date, val weather: String, val temp: Int, val wind: Int, val direction: String)
