package com.example.brad.materialstuff

import android.icu.util.ValueIterator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.brad.materialstuff.WeatherModels.Alert
import com.example.brad.materialstuff.WeatherModels.WeatherData
import kotlinx.android.synthetic.main.alert_card_list_item.view.*
import java.text.SimpleDateFormat

class AlertsFragment : Fragment(){

    lateinit var recyclerView: RecyclerView
    lateinit var viewAdapter: AlertAdapter
    lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
    val view = inflater.inflate(R.layout.weekly_layout, container, false)

        viewManager = LinearLayoutManager(context)
        viewAdapter = AlertAdapter(WeatherData().getFakeAlerts(3))

        recyclerView = view.findViewById<RecyclerView>(R.id.weekly_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        return view
    }
}

class AlertAdapter(private var dataset: List<Alert>): RecyclerView.Adapter<AlertAdapter.ViewHolder>(){
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.alert_card_list_item, parent, false) as View
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.date.text = SimpleDateFormat("M/d/YY").format(dataset[position].date).toString()
        holder.view.title.text = dataset[position].title
        holder.view.content.text = dataset[position].description
    }

    override fun getItemCount() = dataset.size
}