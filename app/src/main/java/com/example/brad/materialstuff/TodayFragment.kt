package com.example.brad.materialstuff

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.brad.materialstuff.WeatherModels.WeatherData
import com.example.brad.materialstuff.adapters.HourlyAdapter

class TodayFragment : Fragment(){

    lateinit var recyclerView: RecyclerView
    lateinit var viewAdapter: HourlyAdapter
    lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.today_layout, container, false)

        viewManager = LinearLayoutManager(context)
        viewAdapter = HourlyAdapter(WeatherData().getFakeHourly(24))

        recyclerView = view.findViewById<RecyclerView>(R.id.hourly_recycler_view).apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }

        return view
    }
}