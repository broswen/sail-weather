package com.example.brad.materialstuff

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.example.brad.materialstuff.adapters.CustomPagerAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    lateinit var todayFragment: TodayFragment
    lateinit var weeklyFragment: WeeklyFragment
    lateinit var alertsFragment: AlertsFragment

    lateinit var customPagerAdapter: CustomPagerAdapter
    lateinit var viewPager: ViewPager
    lateinit var bottomNavigationView: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todayFragment = TodayFragment()
        weeklyFragment = WeeklyFragment()
        alertsFragment = AlertsFragment()

        customPagerAdapter = CustomPagerAdapter(listOf(weeklyFragment, todayFragment, alertsFragment), supportFragmentManager)
        viewPager = findViewById(R.id.viewpager)
        viewPager.adapter = customPagerAdapter
        viewPager.addOnPageChangeListener(this)

        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.today
        viewPager.currentItem = 1


        bottomNavigationView.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.today -> {
                    viewPager.currentItem = 1
                    true
                }
                R.id.weekly -> {
                    viewPager.currentItem = 0
                    true
                }
                R.id.alerts -> {
                    viewPager.currentItem = 2
                    true
                }
                else -> {
                    viewPager.currentItem = 1
                    true
                }
            }
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageSelected(position: Int) {
        bottomNavigationView.menu.getItem(position).isChecked = true
    }
}