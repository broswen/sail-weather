package com.example.brad.materialstuff.WeatherModels

import java.util.*

data class DailyWeather(val date: Date, val weather: String, val high: Int, val low: Int, val wind: Int, val direction: String)
