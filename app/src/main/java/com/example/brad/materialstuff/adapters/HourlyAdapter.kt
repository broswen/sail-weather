package com.example.brad.materialstuff.adapters

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.brad.materialstuff.R
import com.example.brad.materialstuff.WeatherModels.HourlyWeather
import kotlinx.android.synthetic.main.hourly_card_list_item.view.*
import java.text.SimpleDateFormat

class HourlyAdapter(private var dataset: List<HourlyWeather>) : RecyclerView.Adapter<HourlyAdapter.ViewHolder>() {
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.hourly_card_list_item, parent, false) as View
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.date.text = SimpleDateFormat("h:mma") .format(dataset[position].date).toString()
        holder.view.weather.text = dataset[position].weather
        holder.view.temp.text = "${dataset[position].temp}F"
        holder.view.wind.text = "${dataset[position].wind}${dataset[position].direction}"
        if(dataset[position].wind > 10){
            holder.view.wind.setTypeface(holder.view.wind.typeface, Typeface.BOLD)
        }
    }

    override fun getItemCount() = dataset.size

}