package com.example.brad.materialstuff.adapters

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.brad.materialstuff.R
import com.example.brad.materialstuff.WeatherModels.DailyWeather
import kotlinx.android.synthetic.main.daily_card_list_item.view.*
import java.text.SimpleDateFormat

class DailyAdapter(private var dataset: List<DailyWeather>) : RecyclerView.Adapter<DailyAdapter.ViewHolder>(){
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.daily_card_list_item, parent, false) as View
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.date.text = SimpleDateFormat("M/d/yy") .format(dataset[position].date).toString()
        holder.view.weather.text = dataset[position].weather
        holder.view.temp.text = "${dataset[position].low}-${dataset[position].high}F"
        holder.view.wind.text = "${dataset[position].wind}${dataset[position].direction}"
        if(position == 0){
            if(holder.view is CardView){
                holder.view.date.setTypeface(holder.view.date.typeface, Typeface.BOLD_ITALIC)
            }
        }
    }

    override fun getItemCount() = dataset.size
}