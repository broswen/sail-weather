package com.example.brad.materialstuff.WeatherModels

import java.util.*

data class Alert(val title: String, val description: String, val date: Date) {
}