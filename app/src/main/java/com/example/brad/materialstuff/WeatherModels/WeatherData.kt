package com.example.brad.materialstuff.WeatherModels

import java.util.*

class WeatherData {

    fun getFakeHourly(count: Int): List<HourlyWeather>{
        val list = mutableListOf<HourlyWeather>()
        var h = 0
        for(i in 0 until count){
            val date = Date()
            date.time += (3600000 * h)
            list.add(HourlyWeather(date, "Sunny", Random().nextInt(10)+70, Random().nextInt(10)+5, "NW"))
            h++
        }
        return list
    }

    fun getFakeWeekly(): List<DailyWeather>{
        val list = mutableListOf<DailyWeather>()
        var h = 0
        for(i in 0 until 7){
            val date = Date()
            date.time += (3600000 * h * 24)
            list.add(DailyWeather(date, "Sunny", Random().nextInt(10)+60, Random().nextInt(10)+75, Random().nextInt(10)+5, "NW"))
            h++
        }
        return list
    }

    fun getFakeAlerts(count: Int): List<Alert>{
        val list = mutableListOf<Alert>()
        for(i in 0 until count){
            list.add(Alert("Alert $count", "This is the alert description.", Date()))
        }

        return list
    }
}